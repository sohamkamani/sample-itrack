const exec = require('child_process')
  .exec;
const path = require('path');

const dbOptions = require('../config/local')
  .connections.mySqlTest;

const dumpFile = path.resolve('./test-data/dbdata.sql');

const command = `mysql --protocol=tcp --host=${dbOptions.host} --user=${dbOptions.user} --password="${dbOptions.password}" --port=${dbOptions.port} --default-character-set=utf8 --database="${dbOptions.database}"  < "${dumpFile}"`;
exec(command, (error, stdout, stderr) => {
  if (error) {
    console.error(`exec error: ${error}`);
    return;
  }
  console.log(`stdout: ${stdout}`);
  console.log(`stderr: ${stderr}`);
  process.exit();
});
