/**
 * Created by sohamchetan on 22/07/15.
 */
//var ModelGen = require('../../api/services/ModelGen.js');
describe('ModelGen', function () {

  describe('generate model attrs', function () {
    var mockConfig = {
      'sno': 1,
      'artifact': 'artifact1',
      'isUsed': 'TRUE',
      'name': 'Executive Summary',
      'status': null,
      'text1': 'a1',
      'text2': 'a2',
      'text3': 'a3',
      'text4': null,
      'text5': null,
      'text6': null,
      'text7': null,
      'text8': null,
      'text9': null,
      'text10': null,
      'flag1': 'bool1',
      'flag2': null,
      'flag3': null,
      'flag4': null,
      'flag5': null
    };

    var mockConfig2 = {
      'sno': 1,
      'artifact': 'artifact2',
      'isUsed': 'TRUE',
      'name': 'a2',
      'parentId': 'artifact1',
      'status': null,
      'text1': 'a1',
      'text2': 'a2',
      'text3': 'a3',
      'text4': null,
      'text5': null,
      'text6': null,
      'text7': null,
      'text8': null,
      'text9': null,
      'text10': null,
      'flag1': 'bool1',
      'flag2': null,
      'flag3': null,
      'flag4': null,
      'flag5': null
    };

    var attrResult = {
      'a1': {
        'size': 1024,
        'type': 'string'
      },
      'a2': {
        'size': 1024,
        'type': 'string'
      },
      'a3': {
        'size': 1024,
        'type': 'string'
      },
      'bool1': {
        type: 'boolean'
      },
      'initiativeId': {
        'model': 'initiative',
        'type': 'integer'
      },
      'sno': {
        'primaryKey': true,
        'type': 'integer',
        'autoIncrement' : true
      }
    };

    var associationResult = {
      'Executive Summary': {
        collection : 'artifact1',
        via : 'initiativeId'
      }
    };

    var artifactAssociationResult = {
      'a2': {
        collection : 'artifact2',
        via : 'parentId'
      }
    };

    it('generates attributes', function () {
      assert.deepEqual(ModelGen.generateModelAttributes(mockConfig), attrResult);
    });

    it('generates table name', function () {
      expect(ModelGen.generateTableName(mockConfig)).to.equal(mockConfig.name);
    });

    it('generates association configuration', function(){
      assert.deepEqual(ModelGen.generateAssociations([mockConfig]), associationResult);
    });

    it('generates associations for artifacts', function(){
      assert.deepEqual(ModelGen.generateArtifactAssociations(mockConfig, [mockConfig, mockConfig2]), artifactAssociationResult);
    });
  });

});
