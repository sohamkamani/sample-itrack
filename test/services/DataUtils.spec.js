/**
 * Created by sohamchetan on 28/07/15.
 */

describe('Data Utils', function () {

  var modelConfig = [{
    'sno': 1,
    'artifact': 'artifact1',
    'parentId': null,
    'name': 'Executive Summary'
  }, {
    'sno': 2,
    'artifact': 'artifact2',
    'parentId': 'artifact1',
    'name': 'KPI'
  }];

  var flatData = {
    'Executive Summary': [{
      sno: 1
    }],
    KPI: [{
      parentId: 1,
      sno: 1
    }, {
      parentId: 3,
      sno: 2
    }]
  };

  var nestedData = {
    'Executive Summary': [{
      sno: 1,
      KPI: [{
        parentId: 1,
        sno: 1
      }]
    }],
    KPI: [{
      parentId: 1,
      sno: 1
    }, {
      parentId: 3,
      sno: 2
    }]
  };


  describe('get nested pairs', function () {
    it('returns pairs of dependant artifacts', function () {
      var nestedPairs = DataUtils.getNestedPairs(modelConfig);
      assert.deepEqual(nestedPairs, [{
        inner: 'KPI',
        outer: 'Executive Summary'
      }]);
    });
  });

  describe('generate Nested data', function () {
    it('returns nested data frm flat data for each initiative', function () {
      var nestedPairs = DataUtils.getNestedPairs(modelConfig);
      assert.deepEqual(DataUtils.generateNestedData(nestedPairs)(flatData), nestedData);
    });
  });

  describe('map deep', function () {
    var unmapped = [{
      outerKey: [{
        innerKey: 1
      }, {
        innerKey: 2
      }]
    }, {
      outerKey: [{
        innerKey: 3
      }, {
        innerKey: 4
      }]
    }];

    var mapped = [{
      outerKey: [{
        innerKey: 2
      }, {
        innerKey: 3
      }]
    }, {
      outerKey: [{
        innerKey: 4
      }, {
        innerKey: 5
      }]
    }];
    it('deeply performs a mapping function for nested arrays within objects', function () {
      assert.deepEqual(DataUtils.mapDeep(unmapped, 'outerKey', 'innerKey', function (elem) {
        return elem + 1;
      }), mapped);
    });
  });

  describe('transform date', function () {
    it('transforms the date', function () {
      expect(DataUtils.transformDate('01/30/1997'))
        .to.equal('1997-01-29T18:30:00.000Z');
    });

    it('does not transform null', function () {
      expect(DataUtils.transformDate(null))
        .to.equal('Invalid date');
    });


    it('does not transform invalid string', function () {
      expect(DataUtils.transformDate('NaN/NaN/NaN'))
        .to.equal('Invalid date');
    });
  });

});
