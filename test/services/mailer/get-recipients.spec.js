/**
 * Created by sohamchetan on 27/08/15.
 */
'use strict';
describe('Get Recipients', function () {
  describe('initiative owner', function () {
    it('gets initiative owner for a particular initiative id', function (done) {
      Mailer.getRecipients.initiativeOwner(1, function (email) {
        assert.deepEqual(email, ['user8@test.com']);
        done();
      });
    });
    it('returns none if owner doesnt exist', function (done) {
      Mailer.getRecipients.initiativeOwner(35, function (email) {
        assert.deepEqual(email, [null]);
        done();
      });
    });
  });

  describe('initiative owners up', function () {

    it('gets all initiative owners till the tp of the tree', function (done) {
      Mailer.getRecipients.initiativeOwnersUp(13, function (data) {
        assert.deepEqual(data, ['user8@test.com', 'user5@test.com', 'general@test.com']);
        done();
      });
    });

  });
});
