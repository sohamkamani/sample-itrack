'use strict';
import testDataGen from '../../../test-data/mailer-data';
const testData = testDataGen();

describe('Mail Builder', function() {

  describe('newUser', function() {
    it('constructs message for new user login -- user email', function() {
      var user = {
          name: 'test',
          email: 'test@test'
        },
        password = 'test',
        mail = Mailer.mailBuilder.newUser(user, password)
          .userEmail;
      expect(mail.receiver)
        .to.eq(user.email);
      expect(mail.subject)
        .to.eq('Your Account Username');
      expect(mail.message)
        .to.eq('Hi test\n\n\r\rYour account has been set with the following credentials:\n\n\r\remail : test@test. \n\r\n\rPlease follow this link to access the app: itrack\n\r\n\r Your temporary password will be sent to you shortly.');
    });

    it('constructs message for new user login -- password email', function() {
      var user = {
          name: 'test',
          email: 'test@test'
        },
        password = 'test',
        mail = Mailer.mailBuilder.newUser(user, password)
          .passwordEmail;
      expect(mail.receiver)
        .to.eq(user.email);
      expect(mail.subject)
        .to.eq('Your Account Password');
      expect(mail.message)
        .to.eq('Hi test\n\n\r\rYour account has been set with the following temporary password : test\n\r\n\rPlease reset your password on login.');
    });
  });

  describe('Admin change', function() {
    describe('initiative', function() {
      it('forms the body of activity change email', done => {
        Mailer.mailBuilder.adminChangeEmail.initiative({
          oldData: testData.oldData,
          newData: testData.newData.different,
          user: testData.user
        })
          .then(emailBody => assert.deepEqual(emailBody.split('\n'), testData.results.adminInitiativeChange1.split('\n')))
          .then(done, done);
      });

      it('forms the body of activity change email with only 1 change', done => {
        Mailer.mailBuilder.adminChangeEmail.initiative({
          oldData: testData.oldData,
          newData: testData.newData.oneChange,
          user: testData.user
        })
          .then(emailBody => assert.deepEqual(emailBody.split('\n'), testData.results.adminInitiativeChange2.split('\n')))
          .then(done, done);
      });
    });

    describe('kpi', function() {
      it('forms the body of activity change email', done => {
        Mailer.mailBuilder.adminChangeEmail.kpi(testData.kpi)
          .then(emailBody => assert.deepEqual(emailBody.split('\n'), testData.kpi.result.split('\n')))
          .then(done, done);
      });

      it('returns null if data does not change', done => {
        Mailer.mailBuilder.adminChangeEmail.kpi({
          oldData: testData.kpi.oldData,
          newData: testData.kpi.newDataUnchanged,
          user: testData.kpi.user
        })
          .then(emailBody => assert.equal(emailBody, null))
          .then(done, done);
      });
    });

  });

});
