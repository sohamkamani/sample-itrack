// var request = require('supertest');

'use strict';
describe('DataUtils : kpi', function () {

  describe('upsert kpi', function () {
    it('updates kpi if it exists', function (done) {
      DataUtils.kpi.upsertKpi({
          sno: 1
        })
        .then(function (data) {
          expect(data[0].sno)
            .to.equal(1);
          done();
        });
    });

    it('inserts kpi if it doesnt exist', function (done) {
      DataUtils.kpi.upsertKpi({
          kpi: 'test'
        })
        .then(function (data) {
          expect(data.kpi)
            .to.equal('test');
          Artifact2.destroy({
            kpi: 'test'
          }, function (err) {
            if (err) {
              console.log(err);
            }
            done();
          });
        });
    });

  });

  describe('upsert milestone', function () {
    it('updates milestone if it exists', function (done) {
      DataUtils.kpi.upsertMilestone({
          sno: 1
        })
        .then(function (data) {
          expect(data[0].sno)
            .to.equal(1);
          done();
        });
    });

    it('inserts milestone if it doesnt exist', function (done) {
      DataUtils.kpi.upsertMilestone({
          targetValue: '123456',
          initiativeId: -100
        })
        .then(function (data) {
          expect(data.targetValue)
            .to.equal('123456');
          Artifact6.destroy({
            targetValue: '123456'
          }, function (err) {
            if (err) {
              console.log(err);
            }
            done();
          });
        });
    });

  });

  describe('upsert children', function () {
    it('upserts series of kpis', function (done) {
      var mockKpis = [{
        sno: 10
      }, {
        kpi: 'test'
      }, {
        notReallyAField: 'none',
        kpi: 'test'
      }];
      DataUtils.kpi.upsertChildren(mockKpis, {
        parentKpi: 1,
        uom: 'count'
      }, function (err, data) {
        expect(data.length)
          .to.equal(3);
        expect(data[0].uom)
          .to.equal('count');
        done();
      });
    });

    it('deletes kpi if deleted key is true', function () {
      var mockKpis = [{
        kpi: 'test'
      }];
      Artifact2.create(mockKpis)
        .then(function (data) {
          data[0].deleted = true;
          var sno = data[0].sno;
          DataUtils.kpi.upsertChildren(data, 0, '', function (err, deletedData) {
            expect(deletedData[0][0].sno)
              .to.equal(sno);
          });
        });
    });
  });

  describe('upsert milestones', function () {
    it('upserts series of milestones', function (done) {
      var mockMilestones = [{
        sno: 10
      }, {
        targetValue: '123456'
      }, {
        notReallyAField: 'none',
        targetValue: '123456'
      }];
      DataUtils.kpi.upsertMilestones(mockMilestones, {
        parentKpi: 1,
        initiativeId: 1
      }, function (err, data) {
        expect(data.length)
          .to.equal(3);
        done();
      });
    });
  });
});
