'use strict';

describe('Data Utils : Delay Initiative', function() {

  describe('delay Activity', function() {
    it('delays an activity by certain days', function() {
      var originalActivity = {
          plannedStartDate: '01/01/2013',
          actualStartDate: '01/02/2013',
          plannedEndDate: '04/10/2019',
          actualEndDate: '04/29/2019'
        },
        postponedActivity = DataUtils.delayInitiatives.delayActivity(10, '09/18/2015')(originalActivity),
        postponedActivityPast = DataUtils.delayInitiatives.delayActivity(10, '09/18/2012')(originalActivity);
      assert.deepEqual(postponedActivity, {
        plannedStartDate: '01/01/2013',
        actualStartDate: '01/02/2013',
        plannedEndDate: '04/20/2019',
        actualEndDate: '05/09/2019'
      });
      assert.deepEqual(postponedActivityPast, {
        plannedStartDate: '01/11/2013',
        actualStartDate: '01/12/2013',
        plannedEndDate: '04/20/2019',
        actualEndDate: '05/09/2019'
      });
    });
  });

  describe('delay milestone', function(){
    it('delays milestones timePoint', function(){
      var origMilestone ={
        timePoint : '01/02/2019'
      };
      expect(DataUtils.delayInitiatives.delayMilestone(10, '09/18/2015')(origMilestone).timePoint).to.equal('01/12/2019');
      expect(DataUtils.delayInitiatives.delayMilestone(10, '09/18/2020')(origMilestone).timePoint).to.equal('01/02/2019');
    });
  });

});
