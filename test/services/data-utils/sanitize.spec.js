/**
 * Created by sohamchetan on 08/09/15.
 */
'use strict';
var mockInitiatives = require('../../classes/mockInitiatives');

describe('DataUtils: sanitize', function() {

  describe('passwords', function() {
    it('deletes password and temp password keys', function() {
      var data = {
        password: 'lorem',
        tempPassword: 'ipsum',
        dolor: 'sit amet'
      };
      assert.deepEqual(DataUtils.sanitize.passwords(data), {
        dolor: 'sit amet'
      });
    });
  });

  describe('removeNulls', function() {
    it('removes nulls from an array', function() {
      var data = [1, 2, 3, null, null, 4, 5, 6];
      assert.deepEqual(DataUtils.sanitize.removeNulls(data), [1, 2, 3, 4, 5, 6]);
    });
  });

  describe('initiativesFromSkeleton', function() {
    it('returns list of initiative skeletons from raw query data', function() {
      var skeleton = mockInitiatives.initiativeSkeleton;
      assert.deepEqual(DataUtils.sanitize.initiativesFromSkeleton(skeleton), mockInitiatives.expectedSkeleton);
    });
  });

  describe('arrays to objects', function() {
    it('returns first element of an array', function() {
      expect(DataUtils.sanitize.arraysToObjects([{
        lorem: 'ipsum'
      }]).lorem).to.equal('ipsum');
    });

    it('returns object if object', function() {
      expect(DataUtils.sanitize.arraysToObjects({
        lorem: 'ipsum'
      }).lorem).to.equal('ipsum');
    });
  });

});
