'use strict';

describe('data utils private function: removeKey',function(){
  it('should take a clean input and return the same as output',function(){
    const initiative = {
      key:'value',
      key2:'value2'
    };
    const result = DataUtils.activityData.removeKeyTest(initiative);
    assert.deepEqual(result,initiative);
  });

  it('should remove the KPI key from the input',function(){
    const initiative = {
      KPI: [],
      key: 'value'
    };
    const result = DataUtils.activityData.removeKeyTest(initiative);
    assert.deepEqual(result, {
      key: 'value'
    });
  });

  it('should remove password and temp password',function(){
    const initiative = {
      owner:{
        password : 'password',
        tempPassword: 'tempPassword',
        key: 'value'
      },
      key2: 'value2'
    };
    const result = DataUtils.activityData.removeKeyTest(initiative);
    assert.deepEqual(result, {
      owner:{
        key: 'value'
      },
      key2: 'value2'
    });
  });
});
