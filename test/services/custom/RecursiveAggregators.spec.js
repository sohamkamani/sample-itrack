/**
 * Created by sohamchetan on 03/08/15.
 */
'use strict';

var mockInitiatives = require('../../classes/mockInitiatives');

describe('Recursive Aggregators', function () {
  describe('populateActivities', function () {
    var initiatives = [{
      initiativeId: 1,
      activitiesWithStatus: {
        a: 1,
        b: 2
      }
    }, {
      initiativeId: 2,
      parentId: 1,
      activitiesWithStatus: {
        a: 3,
        b: 4
      }
    }, {
      initiativeId: 3,
      parentId: 2,
      activitiesWithStatus: {
        a: 0,
        b: 1
      }
    }, {
      initiativeId: 4,
      parentId: 2,
      activitiesWithStatus: {
        a: 1,
        b: 1
      }
    }, {
      initiativeId: 5,
      parentId: 4,
      activitiesWithStatus: {
        a: 2,
        b: 2
      }
    }];
    it('populates activities', function () {
      Custom.recursiveAggregators.populateActivities(initiatives[0], initiatives);
      expect(initiatives[0].activitiesWithStatus.a)
        .to.equal(7);
      expect(initiatives[0].activitiesWithStatus.b)
        .to.equal(10);
    });
  });

  describe('KPI calculation', function () {
    it('recursively calculates kpi baseline and target', function () {
      var recursiveKpis = mockInitiatives.recursiveKpis;
      Custom.recursiveAggregators.calculateKpiFromChildren(recursiveKpis[0], recursiveKpis);
      expect(recursiveKpis[0].baseline)
        .to.equal(40);
    });
  });


  describe('findSumOfOutcomeKpiMilestones', function () {
    it('finds sum of milestones', function () {
      var milestones = mockInitiatives.mockMilestonesForCalculation;
      assert.deepEqual(Custom.recursiveAggregators.findSumOfOutcomeKpiMilestones(milestones, 7), [{
        timePoint: '12/31/2014',
        targetValue: 80726,
        actualValue: 80726
      }, {
        timePoint: '12/31/2015',
        targetValue: 88751,
        actualValue: 86748
      }, {
        timePoint: '2/28/2015',
        targetValue: 88747,
        actualValue: 86744
      }, {
        timePoint: '6/30/2015',
        targetValue: 88749,
        actualValue: 86746
      }, {
        timePoint: '9/30/2015',
        targetValue: 88750,
        actualValue: 86748
      }]);
    });
    it('finds sum of milestones if one of them is null', function () {
      var milestones = [{
        timePoint: '12/31/2015',
        targetValue: 8,
        actualValue: null
      }, {
        timePoint: '12/31/2015',
        targetValue: 8,
        actualValue: null
      }];
      assert.deepEqual(Custom.recursiveAggregators.findSumOfOutcomeKpiMilestones(milestones, 2), [{
        timePoint: '12/31/2015',
        targetValue: 8,
        actualValue: 0
      }]);
    });
  });

  describe('startKpiTargetJob', function () {
    var data = mockInitiatives.startKpiTargetJobData;
    it('finds resultant milestones of output KPIs', function () {
      var result = Custom.recursiveAggregators.startKpiTargetJob(data.withoutNull)[0].targetBenchmarks;
      assert.deepEqual(result, {
        currentTarget: 116,
        actual: 111,
        timePointTarget: '12/31/2015',
        timePointActual: '12/31/2015'
      });
    });

    it('finds resultant milestones of output KPIs as null if one of the children have not target benchmarks', function () {
      var result = Custom.recursiveAggregators.startKpiTargetJob(data.withNull)[0].targetBenchmarks;
      assert.deepEqual(result, {
        currentTarget: null,
        actual: null,
        timePointTarget: '12/31/2015',
        timePointActual: '12/31/2015'
      });
    });

  });
});
