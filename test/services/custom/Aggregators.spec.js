/**
 * Created by sohamchetan on 30/07/15.
 */
'use strict';

var InitiativeClass = require('../../../api/classes/InitiativeClass'),
  KpiClass = require('../../../api/classes/KpiClass'),
  mockInitiatives = require('../../classes/mockInitiatives'),
  _ = require('lodash');

describe('Custom.aggregators', function () {
  var kpiMilestones = [{
      'targetValue': 100,
      'actualValue': 50,
      'timePoint': '2013-07-11T18:30:00.000Z'
    }, {
      'targetValue': 200,
      'actualValue': 100,
      'timePoint': '2014-07-11T18:30:00.000Z'
    }, {
      'targetValue': 300,
      'actualValue': 300,
      'timePoint': '2018-07-11T18:30:00.000Z'
    }, {
      'targetValue': 400,
      'actualValue': 400,
      'timePoint': '2019-07-11T18:30:00.000Z'
    }],
    kpiMilestonesForActual = [{
      'targetValue': 100,
      'actualValue': 50,
      'timePoint': '2013-07-11T18:30:00.000Z'
    }, {
      'targetValue': 200,
      'timePoint': '2014-07-11T18:30:00.000Z'
    }, {
      'targetValue': 300,
      'actualValue': 300,
      'timePoint': '2018-07-11T18:30:00.000Z'
    }],

    Kpis = [{
      'KPI Milestone': kpiMilestones.slice(0, 2),
      baseline: 1,
      target: 2
    }, {
      'KPI Milestone': kpiMilestones.slice(2, 4),
      baseline: 1,
      target: 2
    }];
  describe('get latest kpi milestone', function () {
    it('gives most recent KPI milestone in future', function () {
      expect(Custom.aggregators.getLatestKpiMilestone(kpiMilestones)
          .timePoint)
        .to.equal('2018-07-11T18:30:00.000Z');
    });

    it('gives most recent KPI milestone in past in case future milestone doesnt exist', function () {
      expect(Custom.aggregators.getLatestKpiMilestone(kpiMilestones.slice(0, 2))
          .timePoint)
        .to.equal('2014-07-11T18:30:00.000Z');
    });
  });

  describe('get nearest actual kpi milestone', function () {
    it('gets the nearest milestone ', function () {
      expect(Custom.aggregators.getNearestPastKpiMilestoneWithActual(kpiMilestonesForActual)
          .timePoint)
        .to.equal('2013-07-11T18:30:00.000Z');
    });
  });

  describe('get kpi average', function () {
    it('gets the average actual to target ratio for given kpi milestones', function () {
      expect(Custom.aggregators.getKpiAvg(kpiMilestones))
        .to.equal(0.75);
    });

    it('doesnt take non numbers into consideration', function () {
      var kpiMilestonesWithNaN = _.clone(kpiMilestones);
      kpiMilestonesWithNaN[3].actualValue = '3%';
      expect(Custom.aggregators.getKpiAvg(kpiMilestones))
        .to.equal(2 / 3);
    });
  });

  describe('get KPI average performance target ratio', function () {
    it('takes in KPIs and gives out ratio number', function () {
      var targetKpis = Kpis.map(function (kpi) {
        return (new KpiClass(kpi))
          .calculateKpiTargets();
      });
      expect(Custom.aggregators.getKpiAveragePerformaceTargetRatio(targetKpis))
        .to.equal(0.49748743718592964);
    });
  });

  describe('get achievement to target ratio', function () {
    var testMilestone1 = {
        actualValue: 3,
        targetValue: 4
      },
      testMilestoneZeroActual = {
        actualValue: 0,
        targetValue: 4
      },
      testMilestoneZeroTarget = {
        actualValue: 4,
        targetValue: 0
      },
      testMilestoneZeroAll = {
        actualValue: 0,
        targetValue: 0
      },
      testKpi = {
        baseline: 1,
        target: 2,
        latestMilestone: testMilestone1,
        nearestActualMilestone: testMilestone1
      },
      testKpiZeroActual = {
        baseline: 0,
        target: 2,
        latestMilestone: testMilestoneZeroActual,
        nearestActualMilestone: testMilestoneZeroActual
      },
      testKpiZeroTarget = {
        baseline: 0,
        target: 2,
        latestMilestone: testMilestoneZeroTarget,
        nearestActualMilestone: testMilestoneZeroTarget
      },
      testKpiZeroAll = {
        baseline: 1,
        target: 2,
        latestMilestone: testMilestoneZeroAll,
        nearestActualMilestone: testMilestoneZeroAll
      },
      testKpiNoBaseline = {
        target: 2,
        latestMilestone: testMilestoneZeroAll,
        nearestActualMilestone: testMilestoneZeroAll
      },
      testKpiInvert = _.clone(testKpi);
    testKpiInvert.baseline = 5;
    it('gets actual/target when baseline < target', function () {
      expect(Custom.aggregators.getAchievementToTargetRatio(testKpi))
        .to.equal(2 / 3);
    });
    it('gets target/actual when baseline > target', function () {
      expect(Custom.aggregators.getAchievementToTargetRatio(testKpiInvert))
        .to.equal(2);
    });
    it('gives zero for zero actual', function () {
      expect(Custom.aggregators.getAchievementToTargetRatio(testKpiZeroActual))
        .to.equal(0);
    });
    it('gives null for zero target', function () {
      expect(Custom.aggregators.getAchievementToTargetRatio(testKpiZeroTarget))
        .to.equal('Infinity');
    });
    it('gives null for zero all', function () {
      expect(Custom.aggregators.getAchievementToTargetRatio(testKpiZeroAll))
        .to.equal(1);
    });
    it('gives garbage if there is no baseline', function () {
      expect(Custom.aggregators.getAchievementToTargetRatio(testKpiNoBaseline))
        .to.equal(null);
    });
  });

  describe('get activity status amount', function () {
    var allInitiatives = [{
      parentId: 1,
      type: 'activity',
      status: 'a'
    }, {
      parentId: 1,
      type: 'activity',
      status: 'a'
    }, {
      parentId: 1,
      type: 'activity',
      status: 'a'
    }, {
      parentId: 1,
      type: 'activity',
      status: 'b'
    }, {
      parentId: 1,
      type: 'activity',
      status: 'b'
    }, {
      parentId: 1,
      type: 'activity',
      status: 'c'
    }];
    it('gets the number of activities of a particular status', function () {
      expect(Custom.aggregators.getActivityStatusAmount(allInitiatives, 1)
          .a)
        .to.equal(3);
      expect(Custom.aggregators.getActivityStatusAmount(allInitiatives, 1)
          .b)
        .to.equal(2);
      expect(Custom.aggregators.getActivityStatusAmount(allInitiatives, 1)
          .c)
        .to.equal(1);
    });
  });

  describe('classify initiative', function () {
    it('classifies initiative to priority', function () {
      expect(Custom.aggregators.classifyInitiative({
            type: ''
          })
          .type)
        .to.equal('priority');
    });
    it('classifies initiative to activity', function () {
      expect(Custom.aggregators.classifyInitiative({
            parentId: 1,
            type: 'activity'
          })
          .type)
        .to.equal('activity');
    });
    it('classifies initiative to priority', function () {
      expect(Custom.aggregators.classifyInitiative({
            parentId: 2,
            type: ''
          })
          .type)
        .to.equal('initiative');
    });
  });

  describe('get initiative status', function () {
    var noneDelayed = {
        Delayed: 1,
        rest: 21
      },
      fewDelayed = {
        Delayed: 2,
        rest: 8
      },
      mostDelayed = {
        Delayed: 6,
        rest: 5
      };
    it('gets initiative status (red) based on kpiratio and activity distribution', function () {
      expect(Custom.aggregators.getInitiativeStatus(0.49, mostDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus(0.49, fewDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus(0.49, noneDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus(0.9, mostDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus(1.1, mostDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus('not a number', mostDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus(null, mostDelayed))
        .to.equal('delayed');
      expect(Custom.aggregators.getInitiativeStatus(0.49, 'nan'))
        .to.equal('delayed');
    });
    it('gets initiative status (blue) based on kpiratio and activity distribution', function () {
      expect(Custom.aggregators.getInitiativeStatus(0.9, fewDelayed))
        .to.equal('onTrack');
      expect(Custom.aggregators.getInitiativeStatus(1.1, fewDelayed))
        .to.equal('onTrack');
      expect(Custom.aggregators.getInitiativeStatus(0.9, noneDelayed))
        .to.equal('onTrack');
      expect(Custom.aggregators.getInitiativeStatus('nan', fewDelayed))
        .to.equal('onTrack');
      expect(Custom.aggregators.getInitiativeStatus(null, fewDelayed))
        .to.equal('onTrack');
      expect(Custom.aggregators.getInitiativeStatus(0.9, 'nan'))
        .to.equal('onTrack');
    });
    it('gets initiative status (green) based on kpiratio and activity distribution', function () {
      expect(Custom.aggregators.getInitiativeStatus(1, noneDelayed))
        .to.equal('completed');
      expect(Custom.aggregators.getInitiativeStatus('not a number', noneDelayed))
        .to.equal('completed');
      expect(Custom.aggregators.getInitiativeStatus(1, 'not a number'))
        .to.equal('completed');
      expect(Custom.aggregators.getInitiativeStatus(null, noneDelayed))
        .to.equal('completed');
    });
    it('gets initiative status (unknown) based on kpiratio and activity distribution', function () {
      expect(Custom.aggregators.getInitiativeStatus('nan', 'nan'))
        .to.equal('unknown');
    });
  });

  describe('get last milestone', function () {
    it('gets the last milestone', function () {
      expect(Custom.aggregators.getLastKpiMilestone(kpiMilestones)
          .timePoint)
        .to.equal('2019-07-11T18:30:00.000Z');
    });
  });

  describe('get Kpi targets', function () {
    it('gets kpi target benchmarks', function () {
      var benchmarks = Custom.aggregators.getKpiTargets((new KpiClass(Kpis[0]))
          .calculateKpiTargets())
        .targetBenchmarks;
      expect(benchmarks.currentTarget)
        .to.equal(200);
      expect(benchmarks.actual)
        .to.equal(100);
      expect(benchmarks.lastTarget)
        .to.equal(200);
    });
  });

  describe('get activity status', function () {
    it('classifies status as completed', function () {
      var completedActivity = {
        plannedStartDate: '2013-07-11T18:30:00.000Z',
        actualEndDate: '2013-08-11T18:30:00.000Z'
      };
      var completedActivity2 = {
        plannedStartDate: '2023-07-21T18:30:00.000Z',
        actualEndDate: '2013-08-11T18:30:00.000Z'
      };
      expect(Custom.aggregators.getActivityStatus(completedActivity))
        .to.equal('completed');
      expect(Custom.aggregators.getActivityStatus(completedActivity2))
        .to.equal('completed');
    });
    it('classifies status as delayed', function () {
      var delayedActivity1 = {
          plannedStartDate: '2013-07-11T18:30:00.000Z'
        },
        delayedActivity2 = {
          plannedStartDate: '2013-07-11T18:30:00.000Z',
          actualStartDate: '2013-07-10T18:30:00.000Z',
          plannedEndDate: '2013-08-11T18:30:00.000Z'
        };
      expect(Custom.aggregators.getActivityStatus(delayedActivity1))
        .to.equal('delayed');
      expect(Custom.aggregators.getActivityStatus(delayedActivity2))
        .to.equal('delayed');
    });
    it('classifies status as on track', function () {
      var onTrackActivity1 = {
          plannedStartDate: '2015-01-31T18:30:00.000Z',
          plannedEndDate: 'Invalid date',
          actualStartDate: '2014-12-31T18:30:00.000Z',
          actualEndDate: 'Invalid date'
        },
        onTrackActivity2 = {
          plannedStartDate: '2015-08-13T18:30:00.000Z',
          plannedEndDate: '2025-09-14T18:30:00.000Z',
          actualStartDate: '2015-09-10T18:30:00.000Z',
          actualEndDate: 'Invalid date'
        },
        onTrackActivity3 = {
          plannedStartDate: '2025-08-13T18:30:00.000Z',
          actualStartDate: '2015-09-10T18:30:00.000Z'
        };
      expect(Custom.aggregators.getActivityStatus(onTrackActivity1))
        .to.equal('onTrack');
      expect(Custom.aggregators.getActivityStatus(onTrackActivity2))
        .to.equal('onTrack');
      expect(Custom.aggregators.getActivityStatus(onTrackActivity3))
        .to.equal('onTrack');
    });
    it('classifies status as unknown', function () {
      const unknownActivity1 = {
        plannedStartDate: '2025-08-13T18:30:00.000Z',
        plannedEndDate: '2025-09-10T18:30:00.000Z'
      };
      const unknownActivity2 = {
        plannedStartDate: '2025-08-13T18:30:00.000Z'
      };
      expect(Custom.aggregators.getActivityStatus({}))
        .to.equal('unknown');
      expect(Custom.aggregators.getActivityStatus(unknownActivity1))
        .to.equal('unknown');
      expect(Custom.aggregators.getActivityStatus(unknownActivity2))
        .to.equal('unknown');
    });
  });


  describe('get number of days delayed', function () {

    it('gets days delayed for delayed activities', function () {
      var delayedActivity = new InitiativeClass(mockInitiatives.delayedActivity1),
        today = new Date('2015-09-09T06:04:33.104Z'),
        daysDelayed;
      delayedActivity.convertDates();
      daysDelayed = Custom.aggregators.numberOfDaysDelayed(delayedActivity, today);
      expect(daysDelayed)
        .to.equal(220);
    });

    it('gets days delayed for delayed activities from planned start date if actual start date is not given', function () {
      var delayedActivity = new InitiativeClass(mockInitiatives.delayedActivity2),
        today = new Date('2015-09-09T06:04:33.104Z'),
        daysDelayed;
      delayedActivity.convertDates();
      daysDelayed = Custom.aggregators.numberOfDaysDelayed(delayedActivity, today);
      expect(daysDelayed)
        .to.equal(220);
    });

    it('gets days delayed for delayed activities from planned end date if actual start date is given', function () {
      var delayedActivity = new InitiativeClass(mockInitiatives.delayedActivity3),
        today = new Date('2015-09-09T06:04:33.104Z'),
        daysDelayed;
      delayedActivity.convertDates();
      daysDelayed = Custom.aggregators.numberOfDaysDelayed(delayedActivity, today);
      expect(daysDelayed)
        .to.equal(8);
    });

    it('gets 0 days delayed for undelayed activities', function () {
      var delayedActivity = new InitiativeClass(mockInitiatives.undelayedActivity),
        today = new Date('2015-09-09T06:04:33.104Z'),
        daysDelayed;
      delayedActivity.convertDates();
      daysDelayed = Custom.aggregators.numberOfDaysDelayed(delayedActivity, today);
      expect(daysDelayed)
        .to.equal(0);
    });
  });
});
