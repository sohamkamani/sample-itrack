'use strict';

import memoizedGetInitiatives from '../../../api/controllers/initiative-controller/get-initiative';
import { expect} from 'chai';
import { spy} from 'sinon';

describe('Memoization', function() {
  it('', () => {
    [Artifact1, Artifact2, Artifact3, Artifact4].forEach((Artifact, i) => describe('refresh cache', () => {

      beforeEach(() => {
        memoizedGetInitiatives.ref.clear();
        spy(Initiative, 'findAndPopulateOne');
      });

      afterEach(() => {
        Initiative.findAndPopulateOne.restore();
      });

      it(`Should clear memoized get initiative on update of article${i + 1}`, done => {
        let createdArtifactId;
        memoizedGetInitiatives(1)
          .then(() => {
            expect(Initiative.findAndPopulateOne.callCount)
              .to.eq(1);
            return Artifact.create({
              initiativeId: 1
            });
          })
          .then(createdArtifact => {
            createdArtifactId = createdArtifact.sno;
            return memoizedGetInitiatives(1);
          })
          .then(() => {
            expect(Initiative.findAndPopulateOne.callCount)
              .to.eq(2);
            return Artifact.update({
              sno: createdArtifactId
            }, {});
          })
          .then(() => memoizedGetInitiatives(1))
          .then(() => {
            expect(Initiative.findAndPopulateOne.callCount)
              .to.eq(3);
            return Artifact.destroy({
              sno: createdArtifactId
            });
          })
          .then(() => memoizedGetInitiatives(1))
          .then(() => {
            expect(Initiative.findAndPopulateOne.callCount)
              .to.eq(4);
          })
          .then(done, done);
      });
    }));
  });
});
