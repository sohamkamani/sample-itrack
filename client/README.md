# Push App

This project is generated with [yo gulp-angular](https://github.com/Swiip/generator-gulp-angular)
version 0.12.1.

## Build & development

Run `gulp build` or `gulp build --env production` for building based on the environment the code is to be deployed.
Run `gulp serve` for preview.

## Testing

Running `gulp test` will run the unit tests with karma.

## Project Details

Dev link - http://dev-sandbox-lx115.amdc.mckinsey.com:1358/dist/#/

## Team

* Vineet (PO)
* Gaurav Kumar (Tech Consultant)
* Soham (Backend Developer)
* Jince (Designer)
* Kakul (Frontend Developer)
* Meenakshi (Frontend Developer)
* Yogish DS (Frontend Developer)
* [Sugan (Frontend Developer)](http://www.iamsugan.in/#/)
