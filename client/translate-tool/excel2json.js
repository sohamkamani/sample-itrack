var Excel = require("exceljs");

var workbook = new Excel.Workbook();

var resultantObject = {};

var fs = require('fs');

workbook.xlsx.readFile(__dirname + '/input/client.xlsx').then(function(work) {
  work.eachSheet(function(sheet) {
    var keyCol = sheet.getColumn(1);
    sheet.eachRow(function(row, rowNumber) {
      if (rowNumber !== 1) {
        if (sheet.name === 'ROOT') {
          if (row.values[3].richText && row.values[3].richText.length > 0) {
            resultantObject[row.values[1]] = row.values[3].richText.map(text => text.text)[0];
          } else {
            resultantObject[row.values[1]] = row.values[3];
          }
        } else {
          resultantObject[sheet.name] = resultantObject[sheet.name] || {};
          if (row.values[3] && row.values[3].richText && row.values[3].richText.length > 0) {
            resultantObject[sheet.name][row.values[1]] = row.values[3].richText.map(text => text.text)[0];
          } else {
            resultantObject[sheet.name][row.values[1]] = row.values[3];
          }
        }
      }
    });
  });
  fs.writeFile("./output/client.json", JSON.stringify(resultantObject), function(err) {
    if (err) {
      return console.log(err);
    }

    console.log("The file was saved!");
  });
});
