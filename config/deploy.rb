# config valid only for current version of Capistrano
# lock '3.4.0'

set :application, 'itrack-saudi'
set :branch, 'saudi'
set :user, 'itrack-saudi'
set :repo_url, 'http://githuben.intranet.mckinsey.com/itrack-farm/itrack-march-2016.git'
set :deploy_to, "/srv/#{fetch :application}"
set :linked_dirs, %w(node_modules projects archives pids log www)

namespace :deploy do
  puts "I am here"
  desc 'Install node modules'
  after :updated, :install_node_modules do
    on roles(:app) do
      within release_path do
        execute :npm, 'install'
      end
    end
  end

  desc 'Copy local.js'
  after :updated, :copy_itrack_config do
    on roles(:app) do
      within release_path do
        puts 'shared path : #{shared_path}'
        execute :cp, "#{shared_path}/local.js", 'config/local.js'
      end
    end
  end

end

namespace :test do
  task :upload do
    on roles(:app) do
      within "#{shared_path}" do
        puts shared_path
        upload! '/Users/sohamchetan/src/pushapp-backend/config/local.js', "#{shared_path}/temp-local2.js", via: :scp
      end
    end
  end

  task :link_jenkins do
    on roles(:app) do
      within release_path do
        execute :ln, "-s", "/srv/apps/jenkins/workspace/push-app-frontend/dist", "./.tmp/public"
      end
    end
  end
end
