# lock '3.4.0'


set :application, 'itrack-client2'
set :branch, 'master'
set :user, 'itrack'
set :repo_url, 'http://githuben.intranet.mckinsey.com/MDX/push-app-backend.git'
set :deploy_to, "/srv/#{fetch :application}"
set :linked_dirs, %w(node_modules projects archives pids log www)
set :frontend_repo, 'PUSH-APP'

namespace :setup do
  desc 'Install node modules'
  after :updated, :install_node_modules do
    on roles(:app) do
      within release_path do
        execute :npm, 'install', '-s'
      end
    end
  end
end
