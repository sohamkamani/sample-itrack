set :application, 'itrack-gmg'
set :user, 'soham_chetan_kamani'
set :app_name, "itrack-gmg"
# config valid only for current version of Capistrano
# lock '3.4.0'
set :branch, 'gmg'
set :repo_url, 'http://githuben.intranet.mckinsey.com/itrack-farm/itrack-march-2016.git'
set :deploy_to, "/home/soham_chetan_kamani/apps/itrack"
set :linked_dirs, %w(node_modules projects archives pids log www)

namespace :deploy do
  desc 'Restart itrack-server'
  after :finished, :restart do
    on roles(:app) do
      within release_path do
        execute 'echo Restarting your service now.'
        execute "cd #{release_path} && APP_NAME=\"#{fetch :application}\" JOB_SYNC_INTERVAL=15 npm run pm2"
        execute 'echo Deployer Successfully deployed this Application'
      end
    end
  end
end

server '104.199.230.142', user: fetch(:user), roles: %w{web app}
