pwd
echo "Starting deployment..."
echo "Symlinking node_module directory..."
ln -s /opt/app/itrack/node_modules node_modules
echo "Installing additional node modules..."
npm install
echo "Deploying application..."
APP_NAME="itrack" npm run pm2
