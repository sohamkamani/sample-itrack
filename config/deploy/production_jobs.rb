server 'atdc-cxpcssw75-lx01-mgmt.comx.mckinsey.com', user: fetch(:user), roles: %w{web app}
# server 'atdc-cxpcssw75-lx02-mgmt.comx.mckinsey.com', user: fetch(:user), roles: %w{web app}

# config valid only for current version of Capistrano
lock '3.4.0'

set :branch, 'master'
set :user, 'itrack'
set :repo_url, 'http://githuben.intranet.mckinsey.com/itrack-farm/itrack-pa.git'
# set :deploy_to, "/srv/#{fetch :user}"
set :deploy_to, "/opt/apps"
set :linked_dirs, %w(node_modules projects archives pids log www)

namespace :deploy do
  desc 'Install node modules'
  after :updated, :install_node_modules do
    on roles(:app) do
      within release_path do
        execute :npm, 'install', '-s'
      end
    end
  end

  desc 'Copy local.js'
  after :updated, :copy_itrack_config do
    on roles(:app) do
      within release_path do
        puts 'shared path : #{shared_path}'
        execute :cp, "#{shared_path}/local.js", 'config/local.js'
      end
    end
  end

  desc 'Restart itrack-server'
  after :finished, :restart do
    on roles(:app) do
      within release_path do
        execute 'echo Restarting your service now.'
        execute "cd #{release_path} && APP_NAME=\"#{fetch :application}\" npm run pm2"
        execute 'echo Deployer Successfully deployed this Application'
      end
    end
  end
end
