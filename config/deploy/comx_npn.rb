set :application, 'itrack-demo'
set :user, 'itrackmoh'
set :app_name, "itrack-demo"
# config valid only for current version of Capistrano
# lock '3.4.0'
set :branch, 'demo'
set :repo_url, 'http://githuben.intranet.mckinsey.com/itrack-farm/itrack-march-2016.git'
set :deploy_to, "/srv/itrackmoh"
set :linked_dirs, %w(node_modules projects archives pids log www)

namespace :deploy do
  desc 'Restart itrack-server'
  after :finished, :restart do
    on roles(:app) do
      within release_path do
        execute 'echo Restarting your service on comx now.'
        # execute 'sudo /etc/init.d/itrack-saudi stop'
        execute "cd #{release_path} && APP_NAME=\"#{fetch :application}\" JOB_SYNC_INTERVAL=15 npm run pm2"
        execute 'echo Deployer Successfully deployed this Application'
      end
    end
  end
end

server 'amdc-cxncssw75-lx01.inpncomx.mckinsey.com', user: fetch(:user), roles: %w{web app}
