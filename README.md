# Itrack

  A framework to quickly roll-out apps and solutions for Tracking an Implementation Plan beign executed by Delivery Unit (s)

## Environment Setup

### Pre Configured environment

| Sno | Env Name      | Target Platform | Database Used | Session Store | CSRF  | CORS  | Comments      |
|:----|:--------------|:----------------|:--------------|:--------------|:------|:------|:----------    |
|1    |development    |na               |na             |na             |na     |na     |Depends on local.js for all setup  |
|2    |staging-heroku |heroku           |Postgres       |redis          |YES    |YES    |               |
|3    |production-heroku |heroku        |Postgres       |redis          |YES    |YES    |               |
|4    |pr-heroku      |heroku           |Postgres       |redis          |YES    |YES    | Used by Review App generated from PULL requests |
|5    |console-heroku |heroku bash      |Postgres       |redis          |YES    |YES    | Used for tasks like db population |
|6    |staging        |comx-npn         |MySQL          |memory         |YES    |YES    |               |
|7    |production     |comx             |MySQL          |redis          |YES    |YES    |               |

### Local.js configuration for development / ComX machines

1. Download relevant sample local.js template from one of these GISTS based on your need

  - [Minimalistic Setup](https://gist.github.com/mail2gauravkumar/a50ed311a1173bc7c315)
  
  - [Heroku like Setup](https://gist.github.com/mail2gauravkumar/0e6772bc7aa6a7ec675f)

2. Copy above file to ``` config\local.js ``` in your app root folder

3. Edit / Comment / Uncomment above file based on the environment you need to setup
 
4. Use ```development``` as <env-name> in the commands below


## Tools and Services for Dev Machine setup

### Minimalistic
1. Install and run mySQL5 or higher
2. Install Node 4.2.1

### For Production like Test environment
1. Install Node 4.2.1
2. Install and run redis (To test Session management)
3. Install and run PostGres (Heroku production uses PostGres)

## Running on local machine

### Prepare codebase

  ```sh
    
    1. npm install
  
    2. cd client
  
    3. npm install
  
    4. bower install
  
    5. gulp build --env <env_name>
  
    6. cd..
    
  ```

### Start server :

  ```sh
    
    sails lift --environment=<env_name>
    
  ```  
  App will be available on port 4010 (depending on port configured in your local.js file)

### Run tests :

  ```sh
    
    npm test
    
  ```

## Preparing for production

### Front end Build
Make sure that CORS is enabled and front-end code is build to support that.

  ```sh
    
    1. cd client
  
    2. npm install
  
    3. bower install
  
    5. gulp build --env production
  
    6. cd..
    
    7. git commit -m 'Building assets with CORS enabled'
    
    8. Push your code (to relevant repo and branch)
    
  ```

#### Deploying to Heroku

1. <First time only> Get your creds on heroku app [heroku app](https://dashboard.heroku.com/apps/itrack-uat/) 
2. <First time only> Add heroku as a remote to your clone of this repo  
 
 ```sh
     
    git remote add heroku https://git.heroku.com/{{app-name.git}}
    
 ``` 
3.  Commit you changes Push your code

  ```sh
       
      git push heroku master
      
   ```
   
4. Populate Databases by any one of the following ways
  
  - From heroku bash on your console
    
    ```sh
           
        1. heroku run bash --app itrack-uat
        
        2. npm run heroku:rebuildDbFromSailsConsole
        
        3. Kill Bash terminal (ctrl-c)
        
    ```

  - By Using DB Copy command of heroku
  
    ```sh
    
        heroku pg:copy <source-app>::DATABASE_URL DATABASE_URL --app itrack-uat
      
    ```
 
5. [Check app logs](https://papertrailapp.com/systems/itrack-uat/events) and test [application](http://itrack-uat.herokuapp.com/) 

