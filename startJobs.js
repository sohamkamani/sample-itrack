'use strict';

var chalk = require('chalk');
require('sails')
  .load({
    hooks: {
      blueprints: false,
      controllers: false,
      cors: false,
      csrf: false,
      grunt: false,
      http: false,
      i18n: false,
      logger: false,
      //orm: leave default hook
      policies: false,
      pubsub: false,
      request: false,
      responses: false,
      //services: leave default hook,
      session: false,
      sockets: false,
      views: false
    }
  }, function (err, app) {
    //The SailsJS app is ready
    //You can access all your SailsJS Models and Services here
    // User.count({}).then(console.log);

    // console.log('log:',User);
    // Custom.recursiveAggregators.startCron();
    var CronJob = require('cron')
      .CronJob;
    var weeklyEmailJob = new CronJob('0 0 0 */7 * *', function () {
      // Custom.cronJobs.weeklyEmailToOwners();
    }, null, true, 'America/Los_Angeles');

    console.log('Cron job PID :', chalk.green(process.pid));
    var timeMinutes = Number(process.env.JOB_SYNC_INTERVAL) || 7;
    var timeInterval = 1000 * 60 * timeMinutes;
    var jobs = [
      require('./jobs/populate-activities-job'),
      require('./jobs/initiative-achievement-target-job')
    ];
    var startJobs = function () {
      console.log('Cron job started with PID :', chalk.green(process.pid), chalk.white(new Date()));
      jobs.forEach(function (job) {
        job();
      });
      Custom.recursiveAggregators.startCron();
    };
    startJobs();
    setInterval(function () {
      console.log('Cron job starting on : ', new Date());
      startJobs();
    }, timeInterval);
    // startCalculationCronJobs.start();
    // weeklyEmailJob.start();

  });
